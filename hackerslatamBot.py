#!/usr/bin/python3
# -*- coding: utf-8 -*-
# BOT DE HACKERS LATAM
# Autor: ZoRrO
#LICENCIA GPL-3 O SUPERIOR
import telebot # Librería de la API del bot.
from telebot import types # Tipos para la API del bot.
import time # Librería para hacer que el programa que controla el bot no se acabe.
import random # para la funcion random
import urllib.request, json  #para parsear json  y obtener el precio deo bitcoin desde https://coindesk.com
from urllib.request import Request, urlopen
import pickle
import os.path
import sys
import logging
import configparser
config = configparser.ConfigParser()
config.read('config.ini')
API = config['DEFAULT']['api']

try:
    ''' Creamos el objeto de nuestro bot
    '''
    bot = telebot.TeleBot(API)
except:
    ''' Hace el print si hay algun error
    '''
    e = sys.exc_info()
    print(e)

#Apartir de aqui son las funciones definidas por los comandos a los cuales responde el bot, como todos son iguales solo se explica uno

@bot.message_handler(commands=['start']) # Indicamos que lo siguiente va a controlar el comando '/star'
def saludo(m):
	''' Envia un saludo a la gente'''
	cid = m.chat.id			# Guardamos el ID de la conversación para poder responder.
	bot.send_message(cid, "Gracias :-)")	# Con la función 'send_message()' del bot, enviamos al ID almacenado el texto que queremos.

@bot.message_handler(commands=['hola'])
def hola(m):
	cid = m.chat.id
	vid = open('img/saludo.richd.mp4', 'rb')
	bot.send_message( cid, 'Ola k ase')
	bot.send_video(cid, vid)
	bot.send_video(cid, "FILEID")

@bot.message_handler(commands=['tira'])
def tira(m):
	cid = m.chat.id
	bot.send_message( cid, 'Pregunta de Tira')

@bot.message_handler(commands=['kosita'])
def kosita(m):
	cid = m.chat.id
	bot.send_message( cid, 'Cuánto amor!!')

@bot.message_handler(commands=['firma'])
def firma(m):
	cid = m.chat.id
	bot.send_message( cid, 'pásame tu credencial de elector de una vez para firmar por Marichuy!')

@bot.message_handler(commands=['milenians'])
def milen(m):
	cid = m.chat.id
	numero = random.randrange(18)		#Genera un numero aleatorio con un rango de digitos
	frases ={1:"esto no es touch???",	#Matris donde se guarda cada respuesta
	2:"se tienen que usar botones???",
	3:"donde se conecta  el cargador???",
	4:"hay que caminar para llegar ???",
	5:"no sale en mi aifon????",
	6:"cual es la contraseña del guifi???",
	7:"Tas ChaV0 Ruk0",
        8:"Pasame tu feis",
        9:"pedian sillas en la escuela???",
        10:"lo vi en yiutuib",
        11:"que es /rtfm ???",
        12:"Pero sin chile",
        13:"buenos días solecito",
        14:"solo quiero ser popular",
        15:"¿donde esta mi bitcoin?",
        16:"¿qué es tonyan?",
	17:"eso es del siglo pasado!",
	18:"uy! en el metro no!",
        19:"sólo escucho música electrónica"
	}
	mensaje = frases[numero]		#Se declara la variable "mensaje" con lo obtenido del contenido en "frases"
	bot.send_message( cid, mensaje)

@bot.message_handler(commands=['popular'])

def popular(m):
	cid = m.chat.id
	bot.send_message( cid, '¿popular? Primero dime quién eres.')

@bot.message_handler(commands=['nsa'])
def nsa(m):
	cid = m.chat.id
	bot.send_message( cid, 'Ese si es Juaquer')

@bot.message_handler(commands=['nsaN'])
def nsa2(m):
	cid = m.chat.id
	bot.send_message( cid, 'Y0 no soy Juaquer')

@bot.message_handler(commands=['noches'])
def nsa2(m):
        cid = m.chat.id
        bot.send_message( cid, 'Descansen 💋💋💋')

@bot.message_handler(commands=['canales'])
def canales(m):
        cid = m.chat.id
        bot.send_message( cid, 'https://t.me/HackLA\nhttps://t.me/libromiespalda\nhttps://t.me/autodefensadigital\nhttps://t.me/SembrarEnElBarrio\n')

@bot.message_handler(commands=['dias'])
def dias(m):
        cid = m.chat.id
        video = open('img/dias.mp4', 'rb')
        bot.send_video(cid, video)
        bot.send_video(cid, "FILEID")

@bot.message_handler(commands=['lurkeo'])
def lurkeo(m):
        cid = m.chat.id
        video = open('img/lurkeo.mp4', 'rb')
        bot.send_video(cid, video)
        bot.send_video(cid, "FILEID")

@bot.message_handler(commands=['nerd'])
def nerd(m):
	cid = m.chat.id
	sti = open('img/nerd.jpg', 'rb')
	bot.send_sticker(cid, sti)
	bot.send_sticker(cid, "FILEID")

@bot.message_handler(commands=['colimita'])
def colimita(m):
        cid = m.chat.id
        message = "El glorioso vulcán de Colima se ha quedado afuera de estado de Colima."
        bot.send_message(cid, message)

@bot.message_handler(commands=['cdmx'])
def cdmx(m):
        cid = m.chat.id
        bot.send_message( cid, 'Las provincias también existen!!')

@bot.message_handler(commands=['chavorruco'])
def chavorruco(m):
        cid = m.chat.id
        numero = random.randrange(8)           #Genera un numero aleatorio con un rango de digitos
        frases ={1:"antes sí que era bonito",       #Matris donde se guarda cada respuesta
        2:"los jóvenes van muy deprisa",
        3:"en mis tiempos era el grunge",
	4:"aquí siempre se ha hecho así",
	5:"vamos como locos!",
	6:"sabe el diablo más por viejo que por diablo",
	7:"estas nuevas generaciones no saben...",
	8:"que no conoces qué es un disquette??"
        }
        mensaje = frases[numero]                #Se declara la variable "mensaje" con lo obtenido del contenido en "frases"
        bot.send_message( cid, mensaje)

@bot.message_handler(commands=['mezcal'])
def mezcal(m):
	cid = m.chat.id
	numero = random.randrange(3)
	gif = {
	0:open('img/mezcal1.mp4', 'rb'),
	1:open('img/mezcal2.mp4', 'rb'),
	2:open('img/mezcal3.mp4', 'rb'),
	}
	video = gif[numero]
	bot.send_video(cid, video)
	bot.send_video(cid, "FILEID")

@bot.message_handler(commands=['troll'])
def troll(m):
	cid = m.chat.id
	video = open ('img/troll.mp4', 'rb')
	bot.send_video(cid, video)
	bot.send_video(cid, "FILEID")

@bot.message_handler(commands=['trampa'])
def trampa(m):
        cid = m.chat.id
        vid = open('img/trampaa.gif', 'rb')
        bot.send_video(cid, vid)
        bot.send_video(cid, "FILEID")


@bot.message_handler(commands=['popcorn'])
def palomita(m):
        cid = m.chat.id
        vid = open('img/popc.mp4', 'rb')
        bot.send_video(cid, vid)
        bot.send_video(cid, "FILEID")

####################################################################################################################################################
####################################################################################################################################################
#El comando ayuda, siempre estara al final para que no se pierda entre los nuevos comando que se agregue
#Favor de agregar en esta sección si agregan algun comando nuevo al bot, para mantener actualziado la orden "ayura"
@bot.message_handler(commands=['ayura'])
def ayuda(m):
        cid = m.chat.id
        bot.send_message( cid, "/start - Saludo de Mazorca \n/hola - Saludo\n/nsa - es jueaker \n/nsaN - no soy juaquer \n/milenians - frases milenians al azar\n/tira - pregunta tira\n/rtfm - recomendacion de lectura\n/canales - Listado de canales del grupo \n/popular - ;) \n/lurkeo - gif\n/nerd - jpg \n/dias - gif \n/noches - gif \n/colimita - curiosidad acerca del lugar\n/mezcal - gif\n/trampa - gif\n/troll - gif\n/ayura - Para mostrar este mensaje de nuevo")

bot.polling(none_stop=True)

while True:
    try:
        bot.polling(none_stop=True)
    except Exception as e:
        logger.error(e)
        time.sleep(15)

